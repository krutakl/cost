import {NgModule} from '@angular/core';
import {BrowserModule} from '@angular/platform-browser';

import {AppRoutingModule} from './app-routing.module';
import {AppComponent} from './app.component';
import {RegisterCostComponent} from './components/register-cost/register-cost.component';
import {FilterCostComponent} from './components/filter-cost/filter-cost.component';
import {AverageCostComponent} from './components/average-cost/average-cost.component';
import {SumCostComponent} from './components/sum-cost/sum-cost.component';
import {FormsModule} from "@angular/forms";
import {HttpClientModule} from "@angular/common/http";
import { NgbModule } from '@ng-bootstrap/ng-bootstrap';

@NgModule({
  declarations: [
    AppComponent,
    RegisterCostComponent,
    FilterCostComponent,
    AverageCostComponent,
    SumCostComponent
  ],
  imports: [
    BrowserModule,
    AppRoutingModule,
    FormsModule,
    HttpClientModule,
    NgbModule
  ],
  providers: [],
  bootstrap: [AppComponent]
})
export class AppModule {
}
