import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import {FilterCostComponent} from "./components/filter-cost/filter-cost.component";
import {RegisterCostComponent} from "./components/register-cost/register-cost.component";
import {SumCostComponent} from "./components/sum-cost/sum-cost.component";
import {AverageCostComponent} from "./components/average-cost/average-cost.component";

const routes: Routes = [
  { path: '', redirectTo: 'filter', pathMatch: 'full' },
  { path: 'filter', component: FilterCostComponent },
  { path: 'register', component: RegisterCostComponent },
  { path: 'sum', component: SumCostComponent },
  { path: 'average', component: AverageCostComponent }
];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule]
})
export class AppRoutingModule { }
