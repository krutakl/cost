import { ComponentFixture, TestBed } from '@angular/core/testing';

import { AverageCostComponent } from './average-cost.component';

describe('AverageCostComponent', () => {
  let component: AverageCostComponent;
  let fixture: ComponentFixture<AverageCostComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ AverageCostComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(AverageCostComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
