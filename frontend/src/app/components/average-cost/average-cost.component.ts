import {Component, OnInit} from '@angular/core';
import {Costavg} from "../../models/costavg.model";
import {CostService} from "../../services/cost.service";

@Component({
  selector: 'app-average-cost',
  templateUrl: './average-cost.component.html',
  styleUrls: ['./average-cost.component.css']
})
export class AverageCostComponent implements OnInit {

  avgCosts?: Costavg[]

  constructor(private costService: CostService) {
  }

  ngOnInit(): void {
    this.costService.averageCost()
      .subscribe(
        data => {
          this.avgCosts = data
        },
        error => {
          console.log(error)
        }
      )
  }

}
