import {Component, OnInit, TemplateRef, ViewChild} from '@angular/core';
import {CostService} from "../../services/cost.service";
import {Cost} from "../../models/cost.model";
import {NgbModal} from "@ng-bootstrap/ng-bootstrap";

@Component({
  selector: 'app-register-cost',
  templateUrl: './register-cost.component.html',
  styleUrls: ['./register-cost.component.css']
})
export class RegisterCostComponent implements OnInit {

  value?: number;
  dateTime?: Date;

  @ViewChild('registered') registered?: TemplateRef<any>;

  constructor(private costService: CostService,
              private modalService: NgbModal) {
  }

  ngOnInit(): void {
  }

  registerCost() {
    let cost: Cost = {
      value: this.value,
      dateTime: this.dateTime,
    }
    this.costService.registerCost(cost)
      .subscribe(
        () => {
        },
        error => {
          console.log('registerCost error: ', error);
        },
        () => {
          this.modalService.open(this.registered, {size: 'sm'});
        }
      )
  }

}
