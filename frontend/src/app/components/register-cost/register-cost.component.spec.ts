import { ComponentFixture, TestBed } from '@angular/core/testing';

import { RegisterCostComponent } from './register-cost.component';

describe('RegisterCostComponent', () => {
  let component: RegisterCostComponent;
  let fixture: ComponentFixture<RegisterCostComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ RegisterCostComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(RegisterCostComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
