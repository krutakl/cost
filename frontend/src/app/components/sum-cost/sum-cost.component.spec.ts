import { ComponentFixture, TestBed } from '@angular/core/testing';

import { SumcostComponent } from './sum-cost.component';

describe('SumcostComponent', () => {
  let component: SumcostComponent;
  let fixture: ComponentFixture<SumcostComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ SumcostComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(SumcostComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
