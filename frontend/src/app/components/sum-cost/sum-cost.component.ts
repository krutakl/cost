import {Component, OnInit} from '@angular/core';
import {CostService} from "../../services/cost.service";

@Component({
  selector: 'app-sum-cost',
  templateUrl: './sum-cost.component.html',
  styleUrls: ['./sum-cost.component.css']
})
export class SumCostComponent implements OnInit {

  startTime?: Date;
  endTime?: Date;
  sum?: number;

  constructor(private costService: CostService) {
  }

  ngOnInit(): void {
  }

  sumCosts() {
    this.costService.sumCost(this.startTime, this.endTime).subscribe(
      data => {
        console.log(data)
        this.sum = data
      },
      error => {
        console.log(error)
      }
    )
  }
}
