import {Component, OnInit} from '@angular/core';
import {CostService} from "../../services/cost.service";
import {Cost} from "../../models/cost.model";

@Component({
  selector: 'app-filter-cost',
  templateUrl: './filter-cost.component.html',
  styleUrls: ['./filter-cost.component.css']
})
export class FilterCostComponent implements OnInit {

  costs?: Cost[];
  startTime?: Date;
  endTime?: Date;

  constructor(private costService: CostService) {
  }

  ngOnInit(): void {
  }

  filterCosts(): void {
    this.costService.filterCost(this.startTime, this.endTime)
      .subscribe(
        data => {
          this.costs = data;
          console.log(data);
        },
        error => {
          console.log(error);
        });
  }

}
