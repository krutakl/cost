import {Injectable} from '@angular/core';
import {HttpClient} from "@angular/common/http";
import {Observable} from "rxjs";
import {Cost} from "../models/cost.model";
import {Costavg} from "../models/costavg.model";

const baseUrl = '/api/cost';

@Injectable({
  providedIn: 'root'
})
export class CostService {

  constructor(private http: HttpClient) {
  }

  registerCost(cost: Cost): Observable<string> {
    return this.http.post(baseUrl, cost, {responseType: 'text'});
  }

  filterCost(startTime: Date | undefined, endTime: Date | undefined): Observable<Cost[]> {
    return this.http.get<Cost[]>(`${baseUrl}?startTime=${startTime}&endTime=${endTime}`);
  }

    sumCost(startTime: Date | undefined, endTime: Date | undefined): Observable<number> {
    return this.http.get<number>(`${baseUrl}/sum?startTime=${startTime}&endTime=${endTime}`);
  }

  averageCost(): Observable<Costavg[]> {
    return this.http.get<Costavg[]>(`${baseUrl}/average`);
  }
}
