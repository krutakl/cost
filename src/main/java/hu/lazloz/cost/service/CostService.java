package hu.lazloz.cost.service;

import hu.lazloz.cost.dao.CostRepository;
import hu.lazloz.cost.dao.UserRepository;
import hu.lazloz.cost.model.CostDto;
import hu.lazloz.cost.model.CostEntity;
import hu.lazloz.cost.model.DailyCostDto;
import hu.lazloz.cost.model.UserEntity;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.security.core.userdetails.UsernameNotFoundException;
import org.springframework.stereotype.Service;

import javax.transaction.Transactional;
import java.time.LocalDateTime;
import java.util.List;
import java.util.stream.Collectors;

@Service
public class CostService {

    private final Logger logger = LoggerFactory.getLogger(CostService.class);

    private final CostRepository costRepository;
    private final UserRepository userRepository;

    public CostService(CostRepository costRepository, UserRepository userRepository) {
        this.costRepository = costRepository;
        this.userRepository = userRepository;
    }

    public void registerCost(CostDto cost) {
        logger.debug("registerCost cost:{}", cost);
        long userId = getCurrentUserId();
        CostEntity costEntity = new CostEntity();
        costEntity.setUserId(userId);
        costEntity.setValue(cost.getValue());
        costEntity.setDateTime(cost.getDateTime());
        costRepository.save(costEntity);
    }

    @Transactional
    public List<CostDto> filterCosts(LocalDateTime startTime, LocalDateTime endTime) {
        logger.debug("filterCosts startTime:{}, endTime:{}", startTime, endTime);
        return costRepository.findByDateTimeBetweenAndUserId(startTime, endTime, getCurrentUserId())
                .map(cost -> new CostDto(cost.getValue(), cost.getDateTime()))
                .collect(Collectors.toList());
    }

    @Transactional
    public long sumCosts(LocalDateTime startTime, LocalDateTime endTime) {
        logger.debug("sumCosts startTime:{}, endTime:{}", startTime, endTime);
        return costRepository.getSumValueByDateTimeBetween(startTime, endTime, getCurrentUserId());
    }

    public List<DailyCostDto> getAverageDailyCosts() {
        logger.debug("getAverageDailyCosts");
        return costRepository.getAverageDailyCostsUsingUserId(getCurrentUserId());
    }

    private long getCurrentUserId() {
        logger.trace("getCurrentUserId");
        Authentication authentication = SecurityContextHolder.getContext().getAuthentication();
        String name = authentication.getName();
        UserEntity user = userRepository.findByName(name)
                .orElseThrow(() -> new UsernameNotFoundException("Username: " + name + " not found"));
        return user.getId();
    }

}
