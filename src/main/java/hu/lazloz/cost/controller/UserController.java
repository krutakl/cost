package hu.lazloz.cost.controller;

import hu.lazloz.cost.service.CostService;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.http.ResponseEntity;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RestController;

@RestController("/api")
public class UserController {

    private final Logger logger = LoggerFactory.getLogger(CostService.class);

    // TODO test and implement
    @PostMapping("/user/register")
    public ResponseEntity<String> registerUser(String username, String password) {
        logger.info("registerUser");
        return ResponseEntity.ok("registerUser");
    }

    @GetMapping("/api")
    public ResponseEntity<String> getRoot() {
        logger.info("getCurrentUserId");
        String username = SecurityContextHolder.getContext().getAuthentication().getName();
        return ResponseEntity.ok("You are logged in as: " + username);
    }
}
