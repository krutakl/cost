package hu.lazloz.cost.controller;

import hu.lazloz.cost.model.CostDto;
import hu.lazloz.cost.model.DailyCostDto;
import hu.lazloz.cost.service.CostService;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.format.annotation.DateTimeFormat;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import javax.validation.Valid;
import java.time.LocalDateTime;
import java.util.List;

@RestController
@RequestMapping("/api/cost")
public class CostController {

    private final Logger logger = LoggerFactory.getLogger(CostService.class);

    private final CostService costService;

    public CostController(CostService costService) {
        this.costService = costService;
    }

    @PostMapping
    public ResponseEntity<String> registerCost(@Valid @RequestBody CostDto costDto) {
        logger.info("registerCost");
        costService.registerCost(costDto);
        return ResponseEntity.ok("COST REGISTERED");
    }

    @GetMapping
    public ResponseEntity<List<CostDto>> filterCosts(@RequestParam @DateTimeFormat(iso = DateTimeFormat.ISO.DATE_TIME) LocalDateTime startTime,
                                                     @RequestParam @DateTimeFormat(iso = DateTimeFormat.ISO.DATE_TIME) LocalDateTime endTime) {
        logger.info("filterCosts");
        List<CostDto> costDtos = costService.filterCosts(startTime, endTime);
        return ResponseEntity.ok(costDtos);
    }

    @GetMapping("/sum")
    public ResponseEntity<Long> sumCosts(@RequestParam @DateTimeFormat(iso = DateTimeFormat.ISO.DATE_TIME) LocalDateTime startTime,
                                         @RequestParam @DateTimeFormat(iso = DateTimeFormat.ISO.DATE_TIME) LocalDateTime endTime) {
        logger.info("sumCosts");
        long response = costService.sumCosts(startTime, endTime);
        return ResponseEntity.ok(response);
    }

    @GetMapping("/average")
    public ResponseEntity<List<DailyCostDto>> getAverageCosts() {
        logger.info("getAverageCosts");
        List<DailyCostDto> averageDailyCosts = costService.getAverageDailyCosts();
        return ResponseEntity.ok(averageDailyCosts);
    }

}
