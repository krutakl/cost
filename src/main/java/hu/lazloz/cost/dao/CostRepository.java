package hu.lazloz.cost.dao;

import hu.lazloz.cost.model.CostEntity;
import hu.lazloz.cost.model.DailyCostDto;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.CrudRepository;

import java.time.LocalDateTime;
import java.util.List;
import java.util.stream.Stream;

public interface CostRepository extends CrudRepository<CostEntity, Long> {

    Stream<CostEntity> findByDateTimeBetweenAndUserId(LocalDateTime startTime, LocalDateTime endTime, long userId);

    @Query("SELECT COALESCE(SUM(c.value),0) from CostEntity c WHERE c.dateTime BETWEEN ?1 AND ?2 AND c.userId = ?3")
    long getSumValueByDateTimeBetween(LocalDateTime startTime, LocalDateTime endTime, long userId);

    @Query("SELECT new hu.lazloz.cost.model.DailyCostDto(SUBSTRING(c.dateTime, 1, 10), AVG(c.value)) from CostEntity c GROUP BY SUBSTRING(c.dateTime, 1, 10) HAVING c.userId = ?1 ")
    List<DailyCostDto> getAverageDailyCostsUsingUserId(long currentUserId);

    long countByUserId(long userId);
}
