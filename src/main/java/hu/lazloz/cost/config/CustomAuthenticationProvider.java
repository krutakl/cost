package hu.lazloz.cost.config;

import hu.lazloz.cost.dao.UserRepository;
import hu.lazloz.cost.model.UserEntity;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.security.authentication.AuthenticationProvider;
import org.springframework.security.authentication.BadCredentialsException;
import org.springframework.security.authentication.UsernamePasswordAuthenticationToken;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.AuthenticationException;
import org.springframework.security.crypto.bcrypt.BCryptPasswordEncoder;
import org.springframework.security.crypto.password.PasswordEncoder;

import javax.annotation.PostConstruct;
import java.util.Collections;
import java.util.Optional;

@Configuration
public class CustomAuthenticationProvider implements AuthenticationProvider {

    private final UserRepository userRepository;
    private final CustomInitialization customInitialization;

    public CustomAuthenticationProvider(UserRepository userRepository, CustomInitialization customInitialization) {
        this.userRepository = userRepository;
        this.customInitialization = customInitialization;
    }

    @Override
    public Authentication authenticate(Authentication authentication) throws AuthenticationException {
        String username = authentication.getName();
        Optional<UserEntity> userEntity = userRepository.findByName(username);
        CharSequence password = (CharSequence) authentication.getCredentials();

        if (userEntity.isPresent() && passwordEncoder().matches(password, userEntity.get().getPasswordHash())) {
            return new UsernamePasswordAuthenticationToken(username, password, Collections.emptyList());
        } else {
            throw new BadCredentialsException("External system authentication failed");
        }
    }

    @Override
    public boolean supports(Class<?> auth) {
        return auth.equals(UsernamePasswordAuthenticationToken.class);
    }

    @Bean
    public PasswordEncoder passwordEncoder() {
        return new BCryptPasswordEncoder();
    }

    @PostConstruct
    public void init() {
        if (customInitialization.isEnabled()) {
            var userEntity = new UserEntity();
            userEntity.setName(customInitialization.getSecurity().getUsername());
            userEntity.setPasswordHash(passwordEncoder().encode(customInitialization.getSecurity().getPassword()));
            userRepository.save(userEntity);
        }
    }

}
