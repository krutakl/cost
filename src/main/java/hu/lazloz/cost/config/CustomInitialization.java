package hu.lazloz.cost.config;

import org.springframework.boot.context.properties.ConfigurationProperties;

@ConfigurationProperties("custom-initialization")
public class CustomInitialization {

    private boolean enabled;

    private final Security security = new Security();

    public boolean isEnabled() {
        return enabled;
    }

    public void setEnabled(boolean enabled) {
        this.enabled = enabled;
    }

    public Security getSecurity() {
        return security;
    }

    public static class Security {

        private String username;
        private CharSequence password;

        public String getUsername() {
            return username;
        }

        public void setUsername(String username) {
            this.username = username;
        }

        public CharSequence getPassword() {
            return password;
        }

        public void setPassword(CharSequence password) {
            this.password = password;
        }
    }

}