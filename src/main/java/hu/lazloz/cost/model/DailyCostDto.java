package hu.lazloz.cost.model;

import java.time.LocalDate;
import java.time.format.DateTimeFormatter;

public class DailyCostDto {
    private LocalDate date;
    private double averageValue;

    public DailyCostDto(String date, double averageValue) {
        this.date = LocalDate.parse(date, DateTimeFormatter.ofPattern("yyyy-MM-dd"));
        this.averageValue = averageValue;
    }

    public LocalDate getDate() {
        return date;
    }

    public void setDate(LocalDate date) {
        this.date = date;
    }

    public double getAverageValue() {
        return averageValue;
    }

    public void setAverageValue(double averageValue) {
        this.averageValue = averageValue;
    }
}
