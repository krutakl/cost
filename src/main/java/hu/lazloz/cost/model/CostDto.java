package hu.lazloz.cost.model;

import javax.validation.constraints.NotNull;
import java.time.LocalDateTime;

public class CostDto {
    @NotNull
    private Long value;
    @NotNull
    private LocalDateTime dateTime;

    public CostDto() {
    }

    public CostDto(Long value, LocalDateTime dateTime) {
        this.value = value;
        this.dateTime = dateTime;
    }

    public Long getValue() {
        return value;
    }

    public void setValue(Long value) {
        this.value = value;
    }

    public LocalDateTime getDateTime() {
        return dateTime;
    }

    public void setDateTime(LocalDateTime dateTime) {
        this.dateTime = dateTime;
    }
}
