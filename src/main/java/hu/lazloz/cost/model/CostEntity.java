package hu.lazloz.cost.model;

import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import java.time.LocalDateTime;

@Entity
public class CostEntity {
    private Long id;
    private Long userId;
    private Long value;
    private LocalDateTime dateTime;

    public CostEntity() {
    }

    public CostEntity(Long userId, Long value, LocalDateTime dateTime) {
        this.userId = userId;
        this.value = value;
        this.dateTime = dateTime;
    }

    public void setId(Long id) {
        this.id = id;
    }

    @Id
    @GeneratedValue
    public Long getId() {
        return id;
    }

    public Long getUserId() {
        return userId;
    }

    public void setUserId(Long userId) {
        this.userId = userId;
    }

    public Long getValue() {
        return value;
    }

    public void setValue(Long value) {
        this.value = value;
    }

    public LocalDateTime getDateTime() {
        return dateTime;
    }

    public void setDateTime(LocalDateTime dateTime) {
        this.dateTime = dateTime;
    }
}
