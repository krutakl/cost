package hu.lazloz.cost.controller;

import hu.lazloz.cost.dao.CostRepository;
import hu.lazloz.cost.dao.UserRepository;
import hu.lazloz.cost.model.CostDto;
import hu.lazloz.cost.model.CostEntity;
import hu.lazloz.cost.model.UserEntity;
import org.junit.jupiter.api.AfterAll;
import org.junit.jupiter.api.AfterEach;
import org.junit.jupiter.api.BeforeAll;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.boot.test.web.client.TestRestTemplate;
import org.springframework.boot.web.server.LocalServerPort;
import org.springframework.http.HttpEntity;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.security.crypto.password.PasswordEncoder;
import org.springframework.web.util.UriComponentsBuilder;

import java.time.LocalDateTime;
import java.util.List;

import static org.junit.jupiter.api.Assertions.assertEquals;

@SpringBootTest(webEnvironment = SpringBootTest.WebEnvironment.RANDOM_PORT)
class CostControllerTest {

    @LocalServerPort
    private int port;

    @Autowired
    private TestRestTemplate restTemplate;
    @Autowired
    private CostRepository costRepository;

    @BeforeAll
    static void prepare(@Autowired UserRepository userRepository, @Autowired PasswordEncoder passwordEncoder) {
        var userEntity = new UserEntity();
        userEntity.setName("user");
        userEntity.setPasswordHash(passwordEncoder.encode("secret"));
        userRepository.save(userEntity);
    }

    @Test
    void testRegisterCost() {
        LocalDateTime time = LocalDateTime.of(2021, 1, 1, 1, 1, 1);
        HttpEntity<CostDto> request = new HttpEntity<>(new CostDto(12L, time));
        ResponseEntity<String> response = restTemplate
                .withBasicAuth("user", "secret")
                .postForEntity("http://localhost:" + port + "/api/cost", request, String.class);
        assertEquals(HttpStatus.OK, response.getStatusCode(), "Status code has to be OK");
        long size = costRepository.count();
        Iterable<CostEntity> all = costRepository.findAll();
        all.forEach(c -> {
            assertEquals(12L, c.getValue(), "Cost value has to be 12");
            assertEquals(time, c.getDateTime(), "Cost dateTime has to be " + time);
        });
        assertEquals(1, size, "Costs number has to be 1");
    }

    @Test
    void testRegisterValidationCost() {
        HttpEntity<CostDto> request = new HttpEntity<>(new CostDto(12L, null));
        ResponseEntity<String> response = restTemplate
                .withBasicAuth("user", "secret")
                .postForEntity("http://localhost:" + port + "/api/cost", request, String.class);
        assertEquals(HttpStatus.BAD_REQUEST, response.getStatusCode(), "Status code has to be BAD_REQUEST");
    }

    @Test
    void testFilterCost() {
        UriComponentsBuilder builder = UriComponentsBuilder.fromHttpUrl("http://localhost:" + port + "/api")
                .path("/cost")
                .queryParam("startTime", "2017-01-13T17:09:42.411")
                .queryParam("endTime", "2021-01-13T17:09:42.411");
        ResponseEntity<List> response = restTemplate
                .withBasicAuth("user", "secret")
                .getForEntity(builder.toUriString(), List.class);

        assertEquals(HttpStatus.OK, response.getStatusCode(), "Status code has to be OK");
        assertEquals(0, response.getBody().size(), "Costs list has to be empty");
    }

    @Test
    void testSumCost() {
        UriComponentsBuilder builder = UriComponentsBuilder.fromHttpUrl("http://localhost:" + port + "/api")
                .path("/cost/sum")
                .queryParam("startTime", "2017-01-13T17:09:42.411")
                .queryParam("endTime", "2021-01-13T17:09:42.411");
        ResponseEntity<Long> response = restTemplate
                .withBasicAuth("user", "secret")
                .getForEntity(builder.toUriString(), Long.class);

        assertEquals(HttpStatus.OK, response.getStatusCode(), "Status code has to be OK");
        assertEquals(0, response.getBody(), "Costs sum has to be 0");
    }

    @Test
    void testMissingEndTimeInSumCostValidation() {
        UriComponentsBuilder builder = UriComponentsBuilder.fromHttpUrl("http://localhost:" + port + "/api")
                .path("/cost/sum")
                .queryParam("startTime", "2017-01-13T17:09:42.411");
        ResponseEntity<Object> response = restTemplate
                .withBasicAuth("user", "secret")
                .getForEntity(builder.toUriString(), Object.class);

        assertEquals(HttpStatus.BAD_REQUEST, response.getStatusCode(), "Status code has to be OK");
    }

    @Test
    void testAverageCost() {
        UriComponentsBuilder builder = UriComponentsBuilder.fromHttpUrl("http://localhost:" + port + "/api")
                .path("/cost/average");
        ResponseEntity<List> response = restTemplate
                .withBasicAuth("user", "secret")
                .getForEntity(builder.toUriString(), List.class);

        assertEquals(HttpStatus.OK, response.getStatusCode(), "Status code has to be OK");
        assertEquals(0, response.getBody().size(), "Costs list has to be empty");
    }

    @AfterEach
    void cleanCost(@Autowired CostRepository costRepository) {
        costRepository.deleteAll();
    }

    @AfterAll
    static void cleanUser(@Autowired UserRepository userRepository) {
        userRepository.deleteAll();
    }

}
