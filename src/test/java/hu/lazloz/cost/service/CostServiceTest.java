package hu.lazloz.cost.service;

import hu.lazloz.cost.dao.CostRepository;
import hu.lazloz.cost.dao.UserRepository;
import hu.lazloz.cost.model.CostDto;
import hu.lazloz.cost.model.CostEntity;
import hu.lazloz.cost.model.DailyCostDto;
import hu.lazloz.cost.model.UserEntity;
import org.junit.jupiter.api.AfterAll;
import org.junit.jupiter.api.AfterEach;
import org.junit.jupiter.api.BeforeAll;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.security.core.userdetails.UsernameNotFoundException;
import org.springframework.security.test.context.support.WithMockUser;

import java.time.LocalDateTime;
import java.util.List;

import static org.junit.jupiter.api.Assertions.*;

@SpringBootTest
class CostServiceTest {

    @Autowired
    private CostService costService;
    @Autowired
    private CostRepository costRepository;
    @Autowired
    private UserRepository userRepository;
    private static long registeredUserId;

    private final LocalDateTime firstOfJanuaryTime = LocalDateTime.of(2021, 1, 1, 10, 0);
    private final LocalDateTime fifteenthOfJanuaryTime = LocalDateTime.of(2021, 1, 15, 10, 0);
    private final LocalDateTime firstOfFebruaryTime = LocalDateTime.of(2021, 2, 1, 10, 0);

    @BeforeAll
    static void setUpRegisteredUser(@Autowired UserRepository userRepository) {
        UserEntity savedUser = userRepository.save(new UserEntity("user"));
        registeredUserId = savedUser.getId();
    }

    @Test
    @WithMockUser
    void registerCost() {
        long value = 123L;
        LocalDateTime time = LocalDateTime.of(2021, 1, 2, 3, 4, 5);
        CostDto cost = new CostDto();
        cost.setValue(value);
        cost.setDateTime(time);
        costService.registerCost(cost);
        long resultedCount = costRepository.count();
        Iterable<CostEntity> all = costRepository.findAll();
        all.forEach(c -> {
            assertEquals(value, c.getValue(), "Cost value has to be " + value);
            assertEquals(time, c.getDateTime(), "Cost dateTime has to be " + time);
        });
        assertEquals(1, resultedCount, "Number of registered cost is not 1");
    }

    @Test
    @WithMockUser(username = "peter")
    void registerCostWithUnnknownUser() {
        CostDto cost = new CostDto();
        cost.setValue(123L);
        assertThrows(UsernameNotFoundException.class, () -> {
            costService.registerCost(cost);
        });
    }

    @Test
    @WithMockUser
    void filterAllCost() {
        createTenValueCostsByDate(firstOfJanuaryTime, fifteenthOfJanuaryTime, firstOfFebruaryTime);

        List<CostDto> allCost = costService.filterCosts(firstOfJanuaryTime.minusDays(1), firstOfFebruaryTime.plusDays(1));

        assertEquals(3, allCost.size(), "Number of all filtered cost is not 3");
    }

    @Test
    @WithMockUser
    void filterFebruryCost() {
        createTenValueCostsByDate(firstOfJanuaryTime, fifteenthOfJanuaryTime, firstOfFebruaryTime);

        List<CostDto> februaryCost = costService.filterCosts(firstOfFebruaryTime.minusMinutes(1), firstOfFebruaryTime.plusMinutes(1));

        assertEquals(1, februaryCost.size(), "Number of february filtered cost is not 1");
        assertEquals(firstOfFebruaryTime, februaryCost.get(0).getDateTime(), "February dateTime does not match");
    }

    @Test
    @WithMockUser
    void filterMiddleCost() {
        createTenValueCostsByDate(firstOfJanuaryTime, fifteenthOfJanuaryTime, firstOfFebruaryTime);

        List<CostDto> middleCost = costService.filterCosts(firstOfJanuaryTime.plusDays(1), firstOfFebruaryTime.minusDays(1));

        assertEquals(1, middleCost.size(), "Number of middle filtered cost is not 1");
        assertEquals(fifteenthOfJanuaryTime, middleCost.get(0).getDateTime(), "Middle cost dateTime does not match");
    }

    @Test
    @WithMockUser
    void filterCostByLoggedInUser() {
        createTenValueCostsByDate(firstOfJanuaryTime);

        UserEntity newUser = userRepository.save(new UserEntity("leslie"));
        saveCost(10L, firstOfJanuaryTime, newUser.getId());

        List<CostDto> allCost = costService.filterCosts(firstOfJanuaryTime.minusDays(1), firstOfJanuaryTime.plusDays(1));

        assertEquals(1, allCost.size(), "Number of all filtered cost is not 1");
    }

    @Test
    @WithMockUser
    void sumAllCost() {
        createTenValueCostsByDate(firstOfJanuaryTime, fifteenthOfJanuaryTime, firstOfFebruaryTime);

        long sum = costService.sumCosts(firstOfJanuaryTime.minusDays(1), firstOfFebruaryTime.plusDays(1));

        assertEquals(30, sum, "Sum of costs has to be 30");
    }

    @Test
    @WithMockUser
    void sumLastTwoCost() {
        createTenValueCostsByDate(firstOfJanuaryTime, fifteenthOfJanuaryTime, firstOfFebruaryTime);

        long sum = costService.sumCosts(firstOfJanuaryTime.plusDays(1), firstOfFebruaryTime.plusDays(1));

        assertEquals(20, sum, "Sum of costs has to be 20");
    }

    @Test
    @WithMockUser
    void sumCostByLoggedInUser() {
        createTenValueCostsByDate(firstOfJanuaryTime);

        UserEntity newUser = userRepository.save(new UserEntity("leslie_sum"));
        saveCost(10L, firstOfJanuaryTime, newUser.getId());

        long sum = costService.sumCosts(firstOfJanuaryTime.minusDays(1), firstOfFebruaryTime.plusDays(1));

        assertEquals(10, sum, "Sum of costs has to be 10");
    }

    @Test
    @WithMockUser
    void averageDailyCost() {
        saveCost(10L, firstOfJanuaryTime, registeredUserId);
        saveCost(11L, firstOfJanuaryTime.plusHours(1), registeredUserId);
        saveCost(9L, firstOfJanuaryTime.plusHours(2), registeredUserId);
        // avg: 10

        saveCost(5L, fifteenthOfJanuaryTime, registeredUserId);
        saveCost(1L, fifteenthOfJanuaryTime.plusHours(1), registeredUserId);
        saveCost(3L, fifteenthOfJanuaryTime.plusHours(2), registeredUserId);
        // avg: 3

        List<DailyCostDto> averageDailyCosts = costService.getAverageDailyCosts();

        assertEquals(2, averageDailyCosts.size(), "Average daily cost day has to be 2");
        assertTrue(averageDailyCosts.stream().anyMatch(d -> d.getAverageValue() == 10), "One daily cost has to be 10");
        assertTrue(averageDailyCosts.stream().anyMatch(d -> d.getAverageValue() == 3), "One daily cost has to be 3");
    }

    @Test
    @WithMockUser
    void averageDailyCostByLoggedInUser() {
        saveCost(10L, firstOfJanuaryTime, registeredUserId);
        saveCost(11L, firstOfJanuaryTime.plusHours(1), registeredUserId);
        saveCost(9L, firstOfJanuaryTime.plusHours(2), registeredUserId);
        // avg: 10

        UserEntity newUser = userRepository.save(new UserEntity("leslie_avg"));
        saveCost(5L, fifteenthOfJanuaryTime, newUser.getId());
        saveCost(1L, fifteenthOfJanuaryTime.plusHours(1), newUser.getId());
        saveCost(3L, fifteenthOfJanuaryTime.plusHours(2), newUser.getId());
        // avg: 3

        List<DailyCostDto> averageDailyCosts = costService.getAverageDailyCosts();

        assertEquals(1, averageDailyCosts.size(), "Average daily cost day has to be 2");
        assertTrue(averageDailyCosts.stream().anyMatch(d -> d.getAverageValue() == 10), "One daily cost has to be 10");
        assertTrue(averageDailyCosts.stream().noneMatch(d -> d.getAverageValue() == 3), "None of daily cost should be 3");
    }

    @AfterEach
    void resetCosts() {
        costRepository.deleteAll();
    }

    @AfterAll
    static void resetUser(@Autowired UserRepository userRepository) {
        userRepository.deleteAll();
    }

    private void createTenValueCostsByDate(LocalDateTime... dateTimes) {
        for (LocalDateTime dateTime : dateTimes) {
            saveCost(10L, dateTime, registeredUserId);
        }
    }

    private void saveCost(long cost, LocalDateTime dateTime, long userId) {
        CostEntity firstCostOnJanuary = new CostEntity(userId, cost, dateTime);
        costRepository.save(firstCostOnJanuary);
    }
}
