# Cost register

## Task description

Create a cost register web application using spring boot.

Your application is able to:
* Register costs of a user
    * Filter costs on a time window (date) set by a user
    * Calculate the sum of all costs for a custom set time window (date) set by a user
    * Calculate the average daily costs
* Make sure that you have unit tests that are covering the edge cases
* Make sure your application can be run easily with a gradle or maven command, even in docker
* Push your homework to any kind of web-based source code management system or share it with us packaged
* Extras to consider:
    * Other tests from the test pyramid are welcome
    * Implement basic user authentication

## How to run

If you want to execute the current code to run (frontend with backend):
`./mvnw spring-boot:run`

If you want to execute the docker image (created by gitlab-ci):
`docker-compose up` 

## How to develop

Start [CostApplication](src/main/java/hu/lazloz/cost/CostApplication.java) with IntellijIDEA.

Start [Frontend angular project](frontend/package.json) with `npm run start` (the first time you need to run `npm install` befor start)

In [application.yml](src/main/resources/application.yml) you can configure the user for basic authentication. If you change it, you have to change the angular [proxy.conf](frontend/src/proxy.conf.json) header too.

Default username / password is `laci` / `p4ssw0rd`