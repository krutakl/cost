FROM openjdk:11 AS build

COPY . .

RUN ./mvnw clean package

FROM openjdk:11 as production

COPY --from=build target/*.jar app.jar

ENTRYPOINT ["java","-jar","/app.jar"]
